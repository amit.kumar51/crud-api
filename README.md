# CRUD API

## How to setup application
1. Clone this project
2. Navigate to the project directory
3. Install Node and npm 
4. Set required evn variables for both client and server
5. Run following commands to run app: <br>
   `npm install` or `npm i` for both client and server<br>
   `npm start` for server and `npm run dev` for client
