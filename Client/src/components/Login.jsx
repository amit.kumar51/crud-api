import React from "react";
import { useForm } from "react-hook-form";
import axiosInstance from "../services/axiosInstance.js";
import { useNavigate } from "react-router-dom";

const Login = () => {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();
  const onsumit = async (data) => {
    try {
      const response = await axiosInstance.post("/user/login", data);
      const authtoken = response.data.token;
      localStorage.setItem("auth-token", authtoken);
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="form-container">
      <div className="form-wrapper">
        <div className="heading">Sign In</div>

        <div className="submit-container">
          <form action="" onSubmit={handleSubmit(onsumit)}>
            <div>
              <input type="email" placeholder="Email" {...register("email")} />
            </div>
            <div>
              <input
                type="password"
                placeholder="Password"
                {...register("password")}
              />
            </div>
            <div className="msg">
                If don't have account <span onClick={ () => navigate('/register')}>Sign up</span>
            </div>
            <div>
              <button className="btn">Login</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Login;
