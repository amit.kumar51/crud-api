import React from "react";
import { useForm } from "react-hook-form";
import axiosInstance from "../services/axiosInstance.js";
import { useNavigate, useParams } from "react-router-dom";

const EditBlog = () => {
  const { register, handleSubmit } = useForm();
  const { id } = useParams();
  const navigate = useNavigate();
  const token = localStorage.getItem("auth-token");
  const onsubmit = async (data) => {
    try {
      const response = await axiosInstance.put(`/blog/edit/${id}`, data, {
        headers: {
          "Content-Type": "application/json",
          "auth-token": `${token}`,
        },
      });

      console.log(response.data);
      navigate("/");
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="form-container">
      <div className="form-wrapper">
        <div className="heading">Edit Post</div>
        <div className="submit-container">
          <form action="" onSubmit={handleSubmit(onsubmit)}>
            <div>
              <input type="text" placeholder="Title" {...register("title")} />
            </div>
            <div>
              <input
                type="text"
                placeholder="Description"
                {...register("description")}
              />
            </div>
            <div>
              <input
                type="text"
                placeholder="Image Url"
                {...register("image")}
              />
            </div>
            <div>
              <button className="btn">Edit Blog</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default EditBlog;
