import React, { useEffect, useState } from "react";
import axiosInstance from "../services/axiosInstance";
import { BiEdit } from "react-icons/bi";
import { RiDeleteBin6Fill } from "react-icons/ri";
import { useNavigate } from "react-router-dom";

const Home = () => {
  const [data, setData] = useState([]);
  const navigate = useNavigate();
  const token = localStorage.getItem("auth-token");
  const getData = async () => {
    try {
      const response = await axiosInstance.get("/blog/allposts");
      setData(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const logOut = () => {
    localStorage.removeItem("auth-token");
    navigate("/signin");
  };

  const editPost = (blogId) => {
    navigate(`/editpost/${blogId}`);
  };

  const deletePost = async (blogId) => {
    try {
      const response = await axiosInstance.delete(`/blog/delete/${blogId}`, {
        headers: {
          "Content-Type": "application/json",
          "auth-token": `${token}`,
        },
      });
      console.log(response.data)
      navigate('/');
    } catch (error) {}
  };

  useEffect(() => {
    getData();
  }, [data]);

  return (
    <div>
      <div className="navbar">
        <button onClick={logOut}>LogOut</button>
        <button onClick={() => navigate("/createpost")}>Create Post</button>
      </div>
      <div className="container">
        {data.map((blog) => (
          <div className="card">
            <div className="image">
              <img id="image" src={blog.image} />
            </div>
            <p className="title">{blog.title}</p>
            <p className="description">{blog.description}</p>
            <div className="icons">
              <div className="delete-icon">
                <RiDeleteBin6Fill
                  size={25}
                  color="red"
                  cursor="pointer"
                  onClick={() => deletePost(blog._id)}
                />
              </div>
              <div className="edit-icon">
                <BiEdit
                  size={25}
                  cursor="pointer"
                  onClick={() => editPost(blog._id)}
                />
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Home;
