import React from "react";
import { useForm } from "react-hook-form";
import axiosInstance from "../services/axiosInstance.js";
import { useNavigate } from "react-router-dom";

const Signup = () => {
  const { register, handleSubmit } = useForm();
  const navigate = useNavigate();
  const onsubmit = async (data) => {
    try {
      const response = await axiosInstance.post("/user/register", data);
      console.log(response.data)
      navigate('/signin')
    } catch (error) {
      console.log(error);
    }
  };
  return (
    <div className="form-container">
      <div className="form-wrapper">
        <div className="heading">Register</div>
        <div className="submit-container">
          <form action="" onSubmit={handleSubmit(onsubmit)}>
            <div>
              <input
                type="text"
                placeholder="Full Name"
                {...register("name")}
              />
            </div>
            <div>
              <input type="email" placeholder="Email" {...register("email")} />
            </div>
            <div>
              <input
                type="password"
                placeholder="Password"
                {...register("password")}
              />
            </div>
            <div className="msg">
                If you already have account <span onClick={ () => navigate('/signin')}>Sign in</span>
            </div>
            <div>
              <button className="btn">SignUp</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default Signup;
