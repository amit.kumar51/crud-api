import React from 'react'
import ValidUser from '../utils/ValidUser.js'
import { Navigate } from 'react-router-dom'

const PrivateRoutes = ({component}) => {
    return !ValidUser() ? <Navigate to="/signin"/> : component;
}

export default PrivateRoutes