import React from 'react'
import ValidUser from '../utils/ValidUser.js'
import { Navigate } from 'react-router-dom';

const PublicRoutes = ({component}) => {
  return ValidUser() ? <Navigate to="/" /> : component;
}

export default PublicRoutes