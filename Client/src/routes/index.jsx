import {Routes, Route} from 'react-router-dom'
import EditBlog from '../components/EditBlog'
import React from 'react'
import PrivateRoutes from './PrivateRoutes'
import PublicRoutes from './PublicRoutes'
import Home from '../components/Home'
import Signup from '../components/Signup'
import Login from '../components/Login'
import CreateBlog from '../components/CreateBlog'

const myRoutes = [
    {
        path: "/",
        compnent: <Home/>,
        restricted: true
    },

    {
        path: "/register",
        compnent: <Signup/>,
        restricted: false
    },

    {
        path: "/signin",
        compnent: <Login/>,
        restricted: false
    },

    {
        path: "/createpost",
        compnent: <CreateBlog/>,
        restricted: true
    },

    {
        path: "/editpost/:id",
        compnent: <EditBlog/>,
        restricted: true
    }
]


const RouterComponent = () => {
  return (
    <Routes>
        {myRoutes.map((route, index) => {
            return (
                <Route 
                    key={index}
                    path={route.path}
                    element= {route.restricted ?< PrivateRoutes component={route.compnent}/> : <PublicRoutes component={route.compnent}/>}
                />
            )
        })}
    </Routes>
  )
}

export default RouterComponent