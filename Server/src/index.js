import express from "express";
import dotenv from "dotenv";
import { dbRun } from "./config/db.js";
import postRouter from "./routes/blogPost.js";
import userRouter from "./routes/userRoutes.js";
import cors from 'cors'
const app = express();
dotenv.config();

const port = process.env.PORT || 5000;

app.use(
    cors({
      origin: "*",
    })
  );
  

app.use(express.json({ limit: "10mb" }));
app.use("/blog", postRouter);
app.use("/user", userRouter);

dbRun();

app.listen(port, () => {
    console.log(`Server is running ${port}`);
})