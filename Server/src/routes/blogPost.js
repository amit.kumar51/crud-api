import express from 'express';
import { createPost, deletePost, editPost, getAllPosts } from '../controllers/blogPostController.js';
import userAuth from '../middleware/userAuth.js';
const postRouter = express.Router();

postRouter.post("/create",userAuth, createPost);
postRouter.get("/allposts", getAllPosts);
postRouter.put("/edit/:postId",userAuth, editPost);
postRouter.delete("/delete/:postId", userAuth, deletePost);


export default postRouter;
