import mongoose from "mongoose";
const { Schema } = mongoose;

const blogSchema = new Schema(
  {
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },

    title: String,
    description: String,
    image: String,
  },
  {
    timestamps: true,
  }
);

export default mongoose.model("BlogPost", blogSchema);
