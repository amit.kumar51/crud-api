import mongoose from 'mongoose';

// Connection URI

export const dbRun = () => {
  try {
    mongoose.connect(process.env.DB_URL);
    console.log("DB connected Successfully");
  } catch (error) {
    console.log("Some connection Error");
  }
};

