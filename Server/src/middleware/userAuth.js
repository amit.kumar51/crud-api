import jwt from "jsonwebtoken";

const userAuth = (req, res, next) => {
  const token = req.header("auth-token");
  console.log(token)
  if (!token) res.status(401).send({ error: "Authentication token invalid" });

  try {
    const data = jwt.verify(token, process.env.SECRET_KEY);
    req.user = data.user;
    next();
  } catch (error) {
    res.status(401).send({ error: "Authentication token invalid" });
  }
};

export default userAuth;
