import Post from '../models/postModel.js'

export const createPost = async (req, res) => {
    try {
        const { title, description, image} = req.body;
        //const imgUrl = req.body;
        const userId = req.user.id;
        const post = new Post({
          title,
          description,
          image,
          user: userId,
        });
    
        const savePost = await post.save();
    
        res.status(200).json(savePost);
      } catch (error) {
        return res
          .status(500)
          .send({ error: error?.message || "Internal Server Error" });
      }
    
}

export const getAllPosts = async(req, res) => {
    try {
        const allposts = await Post.find().sort({ _id: -1 });
        //const posts = await Post.find({ user: req.user.id });
        return res.status(200).json(allposts);
      } catch (error) {
        return res.status(500).send({ error: "Internal Server Error" });
      }
    
}

export const editPost = async (req, res) => {
    const { title, description, image } = req.body;
    const {postId} = req.params;
  try {
    const newPost = {};
    if (title) newPost.title = title;
    if (description) newPost.description = description;
    if (image) newPost.image = image;

    let post = await Post.findById(postId);

    if (!post) return res.status(404).send("Post not found");
;

    post = await Post.findByIdAndUpdate(
      postId,
      { $set: newPost },
      { new: true }
    );

    res.status(200).json({ post });
  } catch (error) {
    return res
      .status(500)
      .send({ error: error?.message || "Internal Server Error" });
  }

}

export const deletePost = async(req, res) => {
    try {
        const { postId } = req.params;
        await Post.findByIdAndDelete(postId);
        res.status(200).send("Post Deleted Successfully");
      } catch (error) {
        return res.status(500).send({ error: "Internal Server Error" });
      }
    
}