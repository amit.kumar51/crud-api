import User from "../models/userModel.js";
import bcrypt from "bcryptjs"
import jwt from 'jsonwebtoken'

//Register User Controller
export const register = async (req, res) => {
    try {
        
        let user = await User.findOne({ email: req.body.email });
    
        if (user) {
          return res
            .status(400)
            .json({ error: "Sorry a user with this email already exists" });
        }
    
        const salt = bcrypt.genSaltSync(10);
        const secPass = await bcrypt.hashSync(req.body.password, salt);
    
        user = await User.create({
          name: req.body.name,
          email: req.body.email,
          password: secPass,
        });
    
        
        res.status(200).send("User Created");
      } catch (error) {
        res.status(500).send({ error: error.message || "Internal Server Error" });
      }
}


// Login Controller
export const login = async (req, res) => {
    const { email, password } = req.body;

  try {
    const user = await User.findOne({ email });

    if (!user) throw new Error("Please enter login with correct credentials");

    const passwordCompare = await bcrypt.compare(password, user.password);

    if (!passwordCompare)
      return res
        .status(400)
        .json({ error: "Please try to login with correct credentials" });

    const data = {
      user: {
        id: user.id,
      },
    };

    const authtoken = jwt.sign(data, process.env.SECRET_KEY);
    res.json({ user: user, token:authtoken });
  } catch (error) {
    return res.status(404).send({
      error: true,
      message: error.message || "Internal server error",
    });
  }

}